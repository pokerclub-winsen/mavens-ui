package de.pokerclubwinsen.mavens.admin.security

import de.pokerclubwinsen.mavens.admin.acl.account.AccountsPasswordComponent
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.stereotype.Component


@Component
class MavensAuthenticationProvider(
    private val accountsPasswordComponent: AccountsPasswordComponent
) : AuthenticationProvider {

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication {
        val username = authentication.principal as String? ?: throw BadCredentialsException("username null")
        val password = authentication.credentials?.toString() ?: throw BadCredentialsException("password null")

        return if (accountsPasswordComponent.check(username, password)) {
            val authorities = listOf<GrantedAuthority>()    // here we'll fill in client roles
            UsernamePasswordAuthenticationToken(username, password, authorities)
        } else
            throw BadCredentialsException("Invalid credentials")
    }

    override fun supports(authentication: Class<*>?): Boolean {
        return authentication == UsernamePasswordAuthenticationToken::class.java
    }
}