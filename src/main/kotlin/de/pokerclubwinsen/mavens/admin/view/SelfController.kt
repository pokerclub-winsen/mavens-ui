package de.pokerclubwinsen.mavens.admin.view

import de.pokerclubwinsen.mavens.admin.acl.account.AccountsGetComponent
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import java.security.Principal

@Controller
class SelfController(
    private val accountsGetComponent: AccountsGetComponent
) {

    @GetMapping("/self")
    fun self(model: Model, principal: Principal): String {
        accountsGetComponent.get(principal.name)?.run {
            model.addAttribute("realName", realName)
        }
        return "self"
    }

}