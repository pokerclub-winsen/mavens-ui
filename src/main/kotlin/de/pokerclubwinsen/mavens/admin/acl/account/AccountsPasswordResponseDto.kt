package de.pokerclubwinsen.mavens.admin.acl.account

import com.fasterxml.jackson.annotation.JsonProperty
import de.pokerclubwinsen.mavens.admin.acl.common.Result

data class AccountsPasswordResponseDto(
    @JsonProperty("Result") val result: Result,
    @JsonProperty("Verified") val verified: String?
)
