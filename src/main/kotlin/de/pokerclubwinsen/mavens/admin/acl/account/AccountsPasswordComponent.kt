package de.pokerclubwinsen.mavens.admin.acl.account

import de.pokerclubwinsen.mavens.admin.MavensApiClient
import de.pokerclubwinsen.mavens.admin.acl.common.Result
import org.springframework.stereotype.Component

@Component
class AccountsPasswordComponent(private val mavensApiClient: MavensApiClient) {

    fun check(username: String, password: String): Boolean {
        val (result, verified) = mavensApiClient.execute(
            "AccountsPassword",
            mapOf(
                "Player" to username,
                "PW" to password
            ),
            AccountsPasswordResponseDto::class.java
        ) ?: return false

        return result == Result.Ok && verified == "Yes"
    }
}