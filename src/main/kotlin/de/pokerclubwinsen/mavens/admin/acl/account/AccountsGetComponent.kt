package de.pokerclubwinsen.mavens.admin.acl.account

import de.pokerclubwinsen.mavens.admin.MavensApiClient
import de.pokerclubwinsen.mavens.admin.acl.common.Result
import org.springframework.stereotype.Component

@Component
class AccountsGetComponent(private val mavensApiClient: MavensApiClient) {

    fun get(username: String): AccountsGetResponseDto? {
        val result = mavensApiClient.execute(
            "AccountsGet",
            mapOf(
                "Player" to username,
            ),
            AccountsGetResponseDto::class.java
        )

        return if (result != null && result.result == Result.Ok)
            result
        else
            null
    }
}