package de.pokerclubwinsen.mavens.admin.acl.common

enum class Result {
    Ok, Error
}