package de.pokerclubwinsen.mavens.admin

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import java.time.Duration

/**
 * Central web client for Poker Maven's API access.
 *
 * @see <a href="https://www.briggsoft.com/docs/pmavens/Technical_Interface.htm#APICommands">
 *     Poker Mavens API command reference
 *     </a>
 */
@Component
class MavensApiClient(
    private val webClient: WebClient,
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Value("\${app.apiUrl}")
    var apiUrl: String,
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Value("\${app.apiPassword}")
    private val apiPassword: String
) {

    final fun <T> execute(command: String, parameters: Map<String, String>, responseType: Class<T>): T? {
        val map = LinkedMultiValueMap<String, String>().apply {
            add("Password", apiPassword)
            add("Command", command)
            add("JSON", "Yes")
        }
        for ((key, value) in parameters) {
            map.add(key, value)
        }

        return webClient.post()
            .uri(apiUrl)
            .body(BodyInserters.fromFormData(map))
            .exchangeToMono { it.toEntity(responseType) }
            .block(Duration.ofSeconds(5))
            ?.body
    }
}