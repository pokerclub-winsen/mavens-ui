package de.pokerclubwinsen.mavens.admin

import com.github.tomakehurst.wiremock.client.WireMock
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.core.env.Environment

const val enableWireMockStubs = true    // set to false if requests should not use WireMock server
const val recordWireMockStubs = false   // set to true if WireMock proxy mode should record HTTP calls as stub files

@SpringBootTest(properties = ["app.apiUrl=http://localhost:8087/api", "app.apiPassword=mavens-api-password"])
@AutoConfigureWireMock(port = 0)
class AbstractWireMockTest {

    @Autowired
    lateinit var environment: Environment

    @Autowired
    lateinit var mavensApiClient: MavensApiClient

    @BeforeEach
    private fun setup() {
        if (enableWireMockStubs) {
            this.environment.getProperty("wiremock.server.port")?.let { port ->
                mavensApiClient.apiUrl = mavensApiClient.apiUrl.replace(Regex("\\d+"), port)
            }
        }
    }

    @BeforeEach
    private fun startRecord() {
        if (recordWireMockStubs) {
            WireMock.startRecording(
                WireMock.recordSpec()
                    .forTarget(mavensApiClient.apiUrl.replace("/api", ""))
                    .makeStubsPersistent(true)
                    .extractTextBodiesOver(0)
            )
        }
    }

    @AfterEach
    private fun stopRecord() {
        if (recordWireMockStubs) {
            WireMock.stopRecording()
        }
    }

}
