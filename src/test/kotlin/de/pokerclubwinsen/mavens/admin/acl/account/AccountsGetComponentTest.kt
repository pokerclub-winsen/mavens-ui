package de.pokerclubwinsen.mavens.admin.acl.account

import de.pokerclubwinsen.mavens.admin.AbstractWireMockTest
import de.pokerclubwinsen.mavens.admin.acl.common.Result
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class AccountsGetComponentTest : AbstractWireMockTest() {

    @Autowired
    private lateinit var testee: AccountsGetComponent

    @Test
    fun goodUser() {
        val user = testee.get("good_user")!!

        user.run {
            assertEquals(Result.Ok, result)
            assertEquals("good_user", player)
            assertEquals("adminwinsen, winsen", permissions)
            assertEquals("Marcus K.", realName)
            assertEquals("Male", gender)
            assertEquals("WL", location)
            assertEquals(1000, balance)
            assertEquals(1234, balance2)
        }
    }

    @Test
    fun noPermsUser() {
        val user = testee.get("no_perms")!!

        user.run {
            assertEquals(Result.Ok, result)
            assertEquals("no_perms", player)
            assertEquals("", permissions)
        }
    }

}