package de.pokerclubwinsen.mavens.admin.acl.account

import de.pokerclubwinsen.mavens.admin.AbstractWireMockTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class AccountsPasswordComponentTest : AbstractWireMockTest() {

    @Autowired
    private lateinit var testee: AccountsPasswordComponent

    @Test
    fun passwordCorrect() {
        assertTrue(testee.check("good_user", "good_pass"))
    }

    @Test
    fun passwordWrong() {
        assertFalse(testee.check("good_user", "bad_pass"))
    }

    @Test
    fun usernameWrong() {
        assertFalse(testee.check("unknown_user", "good_pass"))
    }

}